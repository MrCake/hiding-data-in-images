from PIL import Image
import sys
#image = Image.open("./foxencr.png")
if len(sys.argv) == 2:
    in_image = sys.argv[1]
else:
    in_image = input("Image to decrypt  :") 

image = Image.open(in_image)

width, height = image.size

megstr = ''
for x in range(width):
    for y in range(height):
        pixel = image.getpixel((x,y))
        if len(pixel) == 3:
            r,g,b = image.getpixel( (x,y) ) 
        if len(pixel) == 4:
            r,g,b,a = image.getpixel( (x,y) )
        megstr+= str(r%2)+str(g%2)+str(b%2)

size = int(megstr[0:64],2)

data = []
for i in range(size):
    #print(megstr[64+i*8:64+(i+1)*8])
    data.append(megstr[64+i*8:64+(i+1)*8])


text = ''

for b_char in data:
    o_char = int(b_char,2)
    text+= chr(o_char)
print(text)