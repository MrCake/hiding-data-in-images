from PIL import Image
import sys
#image = Image.open("./fox.jpg")
in_image = ''
if len(sys.argv) == 2:
    in_image = sys.argv[1]
else:
    in_image = input("An image to insert text: ")

image = Image.open(in_image)

#message = "Heloo There!"

message = input("Text to insert: ")
b_list = []
for char in message:
    b_list.append(ord(char))



size = len(message)
b_size = format(size, '#066b')[2:]

b_message = b_size


for byte in b_list:
    b_message+= '{0:08b}'.format(byte)
    #print('{0:08b}'.format(byte))

#print(b_message)

width, height = image.size

if width*height < len(b_message):
    print("Make the image larger or the text smaller")
    exit(1)


colors = range(0,255)
iter = 0
for x in range(width):
    for y in range(height):
        
        pixel = image.getpixel((x,y))
        if len(pixel) == 3:
            r,g,b = image.getpixel( (x,y) ) 
        if len(pixel) == 4:
            r,g,b,a = image.getpixel( (x,y) )

        if iter < len(b_message):
            #red
            if r%2 == 0 and b_message[iter]=='1':
                r = colors[r-1]
            if (r%2 == 1) and (b_message[iter]=='0'):
                r = r-1
            iter+=1
        if iter < len(b_message):
            #green
            if g%2 == 0 and b_message[iter]=='1':
                g = colors[g-1]
            if g%2 == 1 and b_message[iter]=='0':
                g = g-1
            iter+=1
        if iter < len(b_message):
            #blue
            if b%2 == 0 and b_message[iter]=='1':
                b = colors[b-1]
            if b%2 == 1 and b_message[iter]=='0':
                b = b-1
            iter+=1

        image.putpixel( (x,y), (r,g,b,255))

image.save(in_image.split(".")[0]+"encr.png", "PNG" )
